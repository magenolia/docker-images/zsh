#!/usr/bin/env bash

# Build multiplatform images from specified base image.
# Requires being logged to Docker registry.
# Supply base image name as first, mandatory argument.
# Specify optionally a tag regex filter as second argument.

set -euo pipefail

# Get current script directory.
CURRENT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
export CURRENT_DIR

[[ "${#}" -gt 0 ]] || {
  echo "Base image name is expected as first, mandatory argument." >&2
  echo "Specify optionally a tag regex filter as second argument." >&2
  exit 1
}

# Use Buildkit enhanced features.
export DOCKER_BUILDKIT=1

: "${DOCKER_REGISTRY:=${CI_REGISTRY:-docker.io}}"
: "${DOCKER_REGISTRY_USER:=${CI_REGISTRY_USER:-magenolia}}"
: "${DOCKER_REGISTRY_REPO:=${CI_PROJECT_NAME:-$(basename "$(pwd)")}}"
: "${DOCKER_BUILD_PLATFORMS:=${CI_BUILD_PLATFORMS:-linux/amd64,linux/arm64/v8}}"

build_push() {
  local base_image_name base_versions base_version

  base_image_name="${1}"
  tag_regex_filter="${2:-}"

  mapfile -t base_versions < <( "${CURRENT_DIR}/get-image-tags.sh" "${base_image_name}" "${tag_regex_filter}" )

  for base_version in "${base_versions[@]}"; do
    docker buildx build \
      --push \
      --platform "${DOCKER_BUILD_PLATFORMS}" \
      --build-arg BASE_IMAGE="${base_image_name}:${base_version}" \
      --tag "${DOCKER_REGISTRY}/${DOCKER_REGISTRY_USER}/${DOCKER_REGISTRY_REPO}:${base_version}" \
      .
  done
}

build_push "${@}"
